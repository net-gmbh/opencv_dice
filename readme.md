# COUNT DICE EXAMPLE
This app demonstrates dice cube detection and dots counting for each cube. Using OpenCV 3.4.13.

Call:

    ./dicecount [image_name -- Default is ../images/dice-01.jpg]

Using the example image from build ordner call
   
    ./dicecount 

![dicecount](build/dice-05_processed_00.jpg)

While running the application use the trackbar to change image processing parameter. The image is processed once after each change of value. The addional the follow keys inputs are supportet:

- **n** will load the default picture from ./images  

- **s** will save the output image 

- **t** will toggle image prints of image processing time

- **d** will toggle debug prints

- **q** will exit the application

- ESC will exit the app

## Build using cmake
    mkdir build
    cd build
    cmake ..
    make
    

