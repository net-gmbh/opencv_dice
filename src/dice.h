/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *	b.hoffmann - 26.03.2020
 */

#ifndef DICE_H
#define DICE_H

//#define __OPENCV_GUI
#define NO_CANNY_EDGE

#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"

#if  defined __OPENCV_GUI
#include "opencv2/highgui.hpp"
#endif

#include <iostream>
//#include <cmath>
//#include <windows.h>
//#include <time.h>

#include <stdio.h>  // debug only
#include "cv_time_measure.h"

class para_dice
{
public:
	para_dice(int _blursz = 5, int _thd=215, int _canny_thd=20, int _dots_thd = 64, int _dilsz = 1,
		int _dice_min = 55, int _dice_max = 150, float _dice_aratio = 0.15,
		int _dot_min = 4, int _dot_max = 20, float _dot_aradio = 0.25);
	~para_dice();

	void scale_paramter(const float scale);
	inline void scale_paramter(const cv::Size _imgsz)
	{
		*this = para_dice();
		float _scale = _imgsz.height;
		_scale /= 486.0;
		this->scale_paramter(_scale);
	}
	void scale_paramter(int _w, int _h)
	{
		this->scale_paramter(cv::Size(_w,_h));
	}

	inline void init(const para_dice &_para)
	{
		*this = _para;
		this->buildErosionKernel();
		this->buildDilationKernel();
	}

	// --- parameters ---
	int outputImgMode;
	int medianmode;
	int mediansz;
	int mediansigma;

	int threshold_value;
	int edgeThresh;
	int edgeThresh2;

	int erosion_elem;
	int erosion_size;
	//int erosion_mode;
	int dilation_elem;
	int dilation_size;
	int dilation_mode;

	int con_dice_min;
	int con_dice_max;
	int con_dice_asp;

	int con_dots_min;
	int con_dots_max;
	int con_dots_asp;

	int dotsThreshold;

	virtual void buildErosionKernel() {};
	virtual void buildDilationKernel() {};
private:

};




class para_colourseg
{
private:
	const int max_value_H = 360 / 2;
	const int max_value = 255;
protected:
	int thd_low_H, thd_low_S, thd_low_V;
	int thd_high_H, thd_high_S, thd_high_V;

public:
	int set_low_H_thresh(int val);
	int set_high_H_thresh(int val);

	int set_low_S_thresh(int val);
	int set_high_S_thresh(int val);

	int set_low_V_thresh(int val);
	int set_high_V_thresh(int val);

	cv::Scalar get_thd_hight() { return cv::Scalar(thd_high_H, thd_high_S, thd_high_V); }
	cv::Scalar get_thd_low() { return cv::Scalar(thd_low_H, thd_low_S, thd_low_V); }

	para_colourseg(/* args */);
	~para_colourseg();

#if  defined __OPENCV_GUI
	std::string win;
	void init_trackbars(const char * win);
	static void refresh(int val, void * ptr);
#endif

};






class diceRect : public cv::RotatedRect
{
private:
	void print_dice();
public:
	cv::Mat matcropped, matthd;
	cv::Scalar color;
	int dots;
	void setDots(int eyes);
	static cv::Scalar getDiceColor(int dot);

	diceRect();
	diceRect(const cv::Point2f& center, const cv::Size2f& size, float angle);
	diceRect(const cv::Point2f& point1, const cv::Point2f& point2, const cv::Point2f& point3);
	diceRect(const RotatedRect &rec);
	~diceRect();
};




class diceRectVec : public std::vector<diceRect>
{
private:
	static int find_minAreaRect(const vector<vector<cv::Point>>& contours, diceRectVec &diceRects, int min, int max, float aspect_max);

public:
	diceRectVec();
	~diceRectVec();

	std::vector<int> occurrence; // frequency of dots 1..6
	static int write_text_line(cv::Mat& im, const char* buffer, int line = 1, cv::Scalar colour = cv::Scalar(0, 255, 0), double fscale = 0.8);

	void find_minAreaRect(vector<vector<cv::Point>>& contours, int min, int max, float aspect_max);
	// For each contour, search the minimum area rectangle and feeds vector class.
	// Process only rectangles that are almost square (aspect_max) and
	// of the right size beween min and max. Area size depends on resolution.

	void counting_dots(const cv::Mat&im, int dotsThreshold, int min, int max, float aspect_max);
	// for each vector item find dots value from image.

	void draw_minAreaRect(cv::Mat &imageOut);
	// draw contour to image

	int write_count(cv::Mat &imageOut, int line = 1);
	// prints number of found dices
	// and dice occurrence to image

	int copy_dices2mat(cv::Mat &imageOut, cv::Size imgSize);

#if  defined __OPENCV_GUI
	std::vector<std::string> v_win;
	void show_dice_images(const std::string &win0 = "dice cropped / thd");
	void closeAllFigures();
	size_t openAllFigures(const std::string &win0);
#endif
};





class diceCounter :
	public para_dice,
	public para_colourseg
{
public:
	diceCounter();
	~diceCounter();

	diceRectVec vdices;
	int process_image(cv::Mat* image, cv::Mat* imageOut);
	int process_image_post(const cv::Mat &im, const cv::Mat &erosion_dst, cv::Mat &imageOut);
	int process_image_pre(const cv::Mat &imageGray, cv::Mat &erosion_dst, cv::Mat &imageOut);

	void buildErosionKernel();
	void buildDilationKernel();

	cv_time_class mv_time;
	cv::Mat blurImage, thdimage, canny_dst, erosion_dst, dilation_dst;
	cv::Mat ErosionElement, DilationElement;

	bool debug_prints, print_test;

protected:
	//cv::Mat ErosionElement, DilationElement;


	int process_image_pre_colour(const cv::Mat &im, cv::Mat &erosion_dst, cv::Mat &imageOut);

};


#endif // DICE_H
