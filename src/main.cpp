/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *	b.hoffmann - 26.03.2020
 */

#include "opencv2/core/utility.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include <stdio.h>
#include <iostream>

#include "dice.h"

using namespace cv;
using namespace std;

typedef diceCounter diceClass;

// test data
int diceCounts_testimage[] = { 7, 8, 2, 7, 3, 6 };
bool print_test = false;
bool debug_prints = false;



const char *window_result = "NET : Dice & Dots counter";


Mat image, imscale, gray, show;
diceClass g_dice;

static void process_image(int, void *);

int scale_val = 1;
Size sz_origin, sz_dest;
void scale(int val, void *)
{
	if (val == 0) val = 1;
	sz_dest.width = sz_origin.width / val;
	sz_dest.height = sz_origin.height / val;

	process_image(val, NULL);
}


#define save_imashow(fig,mat) { if (!mat.empty()) imshow(fig,mat); \
                                else  cout << #mat << " is empty! " << mat.size() << "\n";}

void process_image(int, void *)
{
	int res = 0;

	// convert colour and scale if selected...
	if (sz_origin == sz_dest)
		cvtColor(image, gray, COLOR_BGR2GRAY);
	else // scale down
	{
		resize(image, imscale, sz_dest, cv::INTER_AREA);           
		cvtColor(imscale, gray, COLOR_BGR2GRAY);
	}
	
	// process image by dice class
	res = g_dice.process_image(&gray, &show);
	if (res == -1)
		return;

	// time measure evaluation
	g_dice.mv_time.print_duration();


	// show images:
	if (sz_origin.width > 1200)
	{
		Mat thmp;
		double scale = 600.0 / double(gray.cols);

		resize(show, thmp, Size(), scale, scale, INTER_AREA);
		save_imashow(window_result, thmp);
	}
	else
	{
		save_imashow(window_result, show);
	}

	return;
}

/*
void para_colourseg::refresh(int val, void *ptr)
{
	process_image(val, ptr);
} //*/

static void help(const char **argv)
{
	printf("\nThis app demonstrates dice detection and eye/dote counting\n"
		"Call:\n"
		"    %s [image_name -- Default is ./images/*]\n\n",
		argv[0]);
}
const char *keys =
{
	"{help h||}{@image |dice-01.jpg|input image name}" };



int main(int argc, const char **argv)
{
	help(argv);
	CommandLineParser parser(argc, argv, keys);
	string filename = parser.get<string>(0);
	std::string imgpath; 
#ifdef _MSC_VER
	imgpath = "C:/Users/${USER}/opencv_dice/images/";
#else
	imgpath = "../images/";
#endif
	
	std::vector<std::string> files;
	
	files.push_back("dice-01.jpg");
	files.push_back("dice-02.jpg");
	files.push_back("dice-03.jpg");
	files.push_back("dice-04.jpg");
	files.push_back("dice-05.jpg");
	files.push_back("dice-06.jpg");

	size_t file_idx = 0;
	filename = imgpath + files.at(file_idx++ % files.size());
	//image = imread(samples::findFile(filename), IMREAD_COLOR);
	image = imread((filename), IMREAD_COLOR);
	if (image.empty())
	{
		printf("Cannot read image file: %s\n", filename.c_str());
		help(argv);
		return -1;
	}

	image.copyTo(gray);

	g_dice.scale_paramter(image.size());

	// Create a window
	int win_row = -1;
	namedWindow(window_result, 1);

	const char *win;
	win = window_result;
	sz_origin = image.size();
	createTrackbar("Output Mode: ",
		win, &g_dice.outputImgMode, 5, process_image);
	createTrackbar("Scale", win, &scale_val, 10, scale);

	createTrackbar("Blur: mode \n 0: Gauss \n 1: Median \n 2: blur ",
		win, &g_dice.medianmode, 2, process_image);
	createTrackbar("Blur: Kernel size:\n 2n +1", win, &g_dice.mediansz, 50, process_image);
	createTrackbar("Blur: Sigma", win, &g_dice.mediansigma, 50, process_image);
	createTrackbar("thrd value", win, &g_dice.threshold_value, 255, process_image);
	createTrackbar("Canny threshold1", win, &g_dice.edgeThresh, 128, process_image);


	// dice size controle pixel
	win = window_result;
	int max = 1000;
	createTrackbar("DICE contur size min  \n [px]", win, &g_dice.con_dice_min, max, process_image);
	createTrackbar("DICE contur size max \n [px]", win, &g_dice.con_dice_max, max, process_image);
	createTrackbar("DICE contur ratio var \n [x0.1]", win, &g_dice.con_dice_asp, 100, process_image);

	// dots size control pixel
	createTrackbar("DOTS contur size min \n [px]", win, &g_dice.con_dots_min, max / 12, process_image);
	createTrackbar("DOTS contur size max \n [px]", win, &g_dice.con_dots_max, max / 12, process_image);
	createTrackbar("DOTS contur ratio var \n [x0.1]", win, &g_dice.con_dots_asp, 100, process_image);

	// Show the image
	scale(1, NULL);
	
	// Wait for a key stroke; the same function arranges events processing
	char key = 'p';
	do
	{
		key = waitKey(200);
		if (key == 'n') // next image
		{
			filename = imgpath + files.at(file_idx++ % files.size());
			Mat next_image = imread(filename, IMREAD_COLOR);
			if (next_image.empty())
			{
				printf("Cannot read image file: %s\n", filename.c_str());
				help(argv);
			}
			else
			{
				image = next_image;
				process_image(0, NULL);
			}
		}
		if (key == 'd')
			g_dice.debug_prints = !g_dice.debug_prints;
		if (key == 't')
			g_dice.print_test = !g_dice.print_test;
		if (key == 'q')
			break;
		if (key == 's')
		{
			static int runingnumber = 0;
			size_t pos_dot = filename.find_last_of('.');
			size_t pos_path = filename.find_last_of("/\\");
			if (pos_path == string::npos)
				pos_path = 0;
			else
				pos_path++;

			string s_runingnr = to_string(runingnumber++);
			string s_file = filename.substr(pos_path, pos_dot - pos_path);
			string s_type = filename.substr(pos_dot);
			string s_add = "_processed_" + ((s_runingnr.size() > 1) ? s_runingnr : "0" + s_runingnr);

			string outputfile = s_file + s_add + s_type;
			imwrite(outputfile, show);
			cout << "file saved to: " << outputfile.c_str() << "\n";
		}
		if (key == 'f')
			cout << "filename: " << filename.c_str() << "\n";

	} while (key != char(27));

	return 0;
}
