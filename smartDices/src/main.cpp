/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *	DC - 26.03.2020
 */

#define MAIN            "smartQr"
#define DESCRIPTION     "QR Decoder"
#define VERSION         "2021-02-04"

#include "main.h"
using namespace std;

// --- synview global variables ---
LvSystem    *System       = 0;
appClass    *app          = NULL;


int main( int argc, char** argv )
{
    printf("-----------------------------------------------------------------\n");
    printf("%s: %s  Version: %s\n", MAIN, DESCRIPTION, VERSION);
    printf("-----------------------------------------------------------------\n");

    char c=0;
    unsigned int tNow=0;
	LvStatus SynviewStatus;       

    remove("/home/root/appSpace/stopFlag.txt");

	// **********************************************
	//                Synview Init     
    // **********************************************
    
    string sPathCti;

#  ifdef _DEBUG
    string sCtid = "cti\\sv.gentld.cti";
#  else
    string sCtid = "cti\\sv.gentl.cti";
#  endif

    // --- open lib ---
    printf("[%05d]:%s:: Synview: Opening the library...\n", NOW, MAIN);
    if (LvLibrary::OpenLibrary() != LVSTATUS_OK)
    {
        char Msg[1024];
        printf("[%05d]:%s:: Synview: Opening the library failed\n", NOW, MAIN);
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("[%05d]:%s:: Synview: Error opening Library:%s\n", NOW, MAIN, Msg);
        goto _cleanup_;
    }

    printf("[%05d]:%s:: Synview: Opening the system...\n", NOW, MAIN);
    if (LvSystem::Open("", System) != LVSTATUS_OK)
    {
        char Msg[1024];
        printf("[%05d]:%s:: Synview: Opening the system failed\n", NOW, MAIN);
        LvGetLastErrorMessage(Msg, sizeof(Msg));
        printf("[%05d]:%s:: Synview: Error opening the system:%s\n", NOW, MAIN, Msg);

        // 2nd chance
        string sPath;
        LvLibrary::GetLibInfoStr(LvInfo_BinPath, sPath);
        sPath = sPath+sCtid;
        printf("[%05d]:%s:: Synview: Opening the system: \"%s\"\n", NOW, MAIN, sPath.c_str());
        if (LvSystem::Open(sPath.c_str(), System) != LVSTATUS_OK)
        {
            printf("[%05d]:%s:: Synview: Opening the system failed\n", NOW, MAIN);
            LvGetLastErrorMessage(Msg, sizeof(Msg));
            printf("[%05d]:%s:: Synview: Error opening the system:%s\n", NOW, MAIN, Msg);
            LvLibrary::CloseLibrary();
            goto _cleanup_;
        }
    }
    System->GetString(LvSystem_TLPath, sPathCti);
    printf("[%05d]:%s:: cti file=\"%s\"\n", NOW, MAIN, sPathCti.c_str());


	// **********************************************
	//              Application Init  
	// **********************************************
    
    printf("[%05d]:%s:: Create application class...\n", NOW, MAIN);
    app = new appClass( System );  
    
    SynviewStatus=app->init();
	if (SynviewStatus!=LVSTATUS_OK) {
		goto _cleanup_;
	}

    
    // **********************************************
    //                   Main Loop
    // **********************************************

    printf("press 'q' or 'ESC' to end program\n");

    while(true)
    {
        // --- quit by keypress ---
        OsKeyPressed ( c );          		
		if ((c=='q')||(c=='Q')||(c==27)) break;   		

        // --- quit by file exit ---
        if (FILE *file = fopen("/home/root/appSpace/stopFlag.txt", "r")) {
            fclose(file);
            break;
        }

        // --- quit by application request --- 
        if (app->exitAppFlag) break;
		
		c = 0;    // reset key
        OsSleep(1000);
    }

_cleanup_:

	printf("\n");
    printf("[%05d]:%s:: cleanup\n", NOW, MAIN);
    tNow=OsGetTickCount();

    if (app)
    {		
        printf("[%05d]:%s:: StopAcquisition camera", NOW, MAIN);
        app->iamDevice.StopAcquisition ();
        printf(" [time:%dms]\n", OsGetTickCount()-tNow); tNow = OsGetTickCount();
        
        app->close();
        delete app;        

        printf("[%05d]:%s:: close system", NOW, MAIN);
        LvSystem::Close ( System );
        printf(" [time:%dms]\n", OsGetTickCount()-tNow); tNow = OsGetTickCount();

        printf("[%05d]:%s:: close library", NOW, MAIN);
        LvLibrary::CloseLibrary ();
        printf(" [time:%dms]\n", OsGetTickCount()-tNow);
    }
    
    //OsSleep ( 1000 );
    //getchar();

    printf("[%05d]:%s:: exiting smartQr\n", NOW, MAIN);
    return ( 0 );
}
