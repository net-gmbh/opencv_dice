/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *	b.hoffmann - 26.03.2020
 */

#include "dice.h"

using namespace std;
using namespace cv;

#define PRINTMATPARA( A ) {                                    	\
cout << "  " <<  #A << "  img: Width: " << (A).cols             \
            << " Height:" << (A).rows                           \
            << " BPP:" << (A).elemSize()                        \
            << " PixFmt:" << (A).channels()                     \
            << " type:" << (A).type() << " (" << CV_8UC1 << ") " \
            << " pData:" << (size_t*)(A).data << "\n";           \
}

#define PRINT_DICE( A ) {								     	\
cout << " detect dice: " <<  #A << "  dots: " << (A).dots       \
            << " color: " << (A).color  << "\n";                \
}



para_colourseg::para_colourseg(/* args */)
{
	thd_low_H = 0;
	thd_low_S = 0;
	thd_low_V = 0;

	thd_high_H = max_value_H;
	thd_high_S = max_value;
	thd_high_V = max_value;
}

para_colourseg::~para_colourseg()
{
}

int para_colourseg::set_low_H_thresh(int val)
{
	thd_low_H = min(thd_high_H - 1, val);
	return thd_low_H;
}

int para_colourseg::set_low_S_thresh(int val)
{
	thd_low_S = min(thd_high_S - 1, val);
	return thd_low_S;
}

int para_colourseg::set_low_V_thresh(int val)
{
	thd_low_V = min(thd_high_V - 1, val);
	return thd_low_V;
}


int para_colourseg::set_high_H_thresh(int val)
{
	thd_high_H = max(val, thd_low_H + 1);
	return thd_high_H;
}

int para_colourseg::set_high_S_thresh(int val)
{
	thd_high_S = max(val, thd_low_S + 1);
	return thd_high_S;
}

int para_colourseg::set_high_V_thresh(int val)
{
	thd_high_V = max(val, thd_low_V + 1);
	return thd_high_V;
}

#if  defined __OPENCV_GUI
static void onHl(int val, void *ptr)
{
	para_colourseg *pthis = (para_colourseg*)ptr;
	val = pthis->set_low_H_thresh(val);
	setTrackbarPos("Low H", pthis->win.c_str(), val);
	para_colourseg::refresh(val, ptr);
}

static void onHh(int val, void * ptr)
{
	para_colourseg *pthis = (para_colourseg*)ptr;
	val = pthis->set_high_H_thresh(val);
	setTrackbarPos("High H", pthis->win.c_str(), val);
	para_colourseg::refresh(val, ptr);
}

static void onSl(int val, void *ptr)
{
	para_colourseg *pthis = (para_colourseg*)ptr;
	val = pthis->set_low_S_thresh(val);
	setTrackbarPos("Low S", pthis->win.c_str(), val);
	para_colourseg::refresh(val, ptr);
}

static void onSh(int val, void *ptr)
{
	para_colourseg *pthis = (para_colourseg*)ptr;
	val = pthis->set_high_S_thresh(val);
	setTrackbarPos("High S", pthis->win.c_str(), val);
	para_colourseg::refresh(val, ptr);
}

static void onVl(int val, void *ptr)
{
	para_colourseg *pthis = (para_colourseg*)ptr;
	val = pthis->set_low_V_thresh(val);
	setTrackbarPos("Low V", pthis->win.c_str(), val);
	para_colourseg::refresh(val, ptr);
}

static void onVh(int val, void *ptr)
{
	para_colourseg *pthis = (para_colourseg*)ptr;
	val = pthis->set_high_V_thresh(val);
	setTrackbarPos("High V", pthis->win.c_str(), val);
	para_colourseg::refresh(val, ptr);
}




void para_colourseg::init_trackbars(const char *win)
{
	createTrackbar("Low H", win, &thd_low_H, max_value_H, onHl, this);
	createTrackbar("High H", win, &thd_high_H, max_value_H, onHh, this);
	createTrackbar("Low S", win, &thd_low_S, max_value, onSl, this);
	createTrackbar("High S", win, &thd_high_S, max_value, onSh, this);
	createTrackbar("Low V", win, &thd_low_V, max_value, onVl, this);
	createTrackbar("High V", win, &thd_high_V, max_value, onVh, this);
	this->win = win;
}

/*
void para_colourseg::refresh(int val, void *ptr)
{
  //process_image(val,ptr);
} //*/


#endif






















diceRect::diceRect()
	: cv::RotatedRect()
{
	dots = -1;
	color = getDiceColor(-1);
}

diceRect::diceRect(const Point2f& center, const Size2f& size, float angle)
	: cv::RotatedRect(center, size, angle)
{
	dots = -1;
	color = getDiceColor(-1);
}
diceRect::diceRect(const Point2f& point1, const Point2f& point2, const Point2f& point3)
	: cv::RotatedRect(point1, point2, point3)
{
	dots = -1;
	color = getDiceColor(-1);
}

diceRect::diceRect(const RotatedRect &rec)
	: cv::RotatedRect(rec)
{
	dots = -1;
	color = getDiceColor(-1);
}
diceRect::~diceRect()
{
}

void diceRect::print_dice()
{
	PRINT_DICE( (*this) );
}

void diceRect::setDots(int eyes)
{
	dots = eyes;
	color = getDiceColor(eyes);
}

cv::Scalar diceRect::getDiceColor(int dot)
{
	// defines rectangle color

	switch (dot)
	{
	case 1: return Scalar(0, 0, 255);
	case 2: return Scalar(0, 128, 255);
	case 3: return Scalar(255, 0, 255);
	case 4: return Scalar(255, 255, 0);
	case 5: return Scalar(255, 0, 128);
	case 6: return Scalar(0, 255, 255);

	default:
		break;
	}

	return Scalar(255, 255, 255);
}













int diceRectVec::find_minAreaRect(const vector<vector<Point>>& contours, diceRectVec & diceRects, int min, int max, float aspect_max)
{
	diceRects.clear();

	int area_min = min * min;
	int area_max = max * max;

	for (size_t i = 0; i < contours.size(); i++)
	{
		// For each contour, search the minimum area rectangle
		RotatedRect rect = minAreaRect(contours[i]);

		// Process only rectangles that are almost square and of the right size.
		// Area size depends on webcam resolution. For me is 640x480 and a dice have
		// a side length of about 0.01 of resolution.

		float aspect = float(rect.size.height) / float(rect.size.width);
		aspect = fabs(aspect - 1);


		if ((aspect < aspect_max) && (rect.size.area() > area_min) && (rect.size.area() < area_max))
		{
			// Check if it's a duplicate rect
			bool duplicate = false;
			for (size_t j = 0; j < diceRects.size(); j++)
			{
				float dist = norm(rect.center - diceRects[j].center);
				if (dist < 10)
				{
					duplicate = true;
					break;
				}
			}

			if (!duplicate)
			{
				diceRects.push_back(rect);
			}
		}
	}
	return (int)diceRects.size();
}

int diceRectVec::write_text_line(Mat & im, const char * buffer, int line, Scalar colour, double fscale)
{
	// scale to (640x480 reference):
	Point tpos(20,  line * 30 * im.rows / 480);
	int line_width = 1 * im.rows / 480;

	fscale = fscale * (double)im.rows / 480.0;


	putText(im, buffer, tpos, FONT_HERSHEY_DUPLEX, fscale, colour, line_width, LINE_AA);

	return ++line;
}

diceRectVec::diceRectVec(/* args */)
{
	occurrence.clear();
	for (int j = 0; j < 6; j++)
		occurrence.push_back(0);
}

diceRectVec ::~diceRectVec()
{
}


void diceRectVec::counting_dots(const Mat& im, int dotsThreshold, int min, int max, float aspect_max)
{
	occurrence.clear();
	for (int j = 0; j < 6; j++)
		occurrence.push_back(0);

	//vcolour.clear();
	// remeber the number of dots for each dice rectangel / save colour for rectange frame.



	Mat rotation, rotated, imageGray;

	if (im.channels() != 1)
	{
		string error_message;
		error_message = "void diceRectVec::counting_dots(const Mat& im, ...)";
		error_message += "Mat &im must be at least grayscale images.";
        CV_Error(Error::StsError, error_message);
		return;
	}

	//for (int i = 0; i < this->size(); i++)
	for (diceRect &rect : *this)
	{
		// Extract dice image
		rotation = getRotationMatrix2D(rect.center, rect.angle, 1.0);
		warpAffine(im, rotated, rotation, im.size(), INTER_CUBIC); //
		getRectSubPix(rotated, Size(rect.size.width - 10, rect.size.height - 10), rect.center, rect.matcropped);

		if (!rect.matcropped.empty())
		{
			// Find contours
			vector<vector<Point>> dieContours;
			vector<Vec4i> dieHierarchy;
			threshold(rect.matcropped, rect.matthd, dotsThreshold, 255, THRESH_BINARY);
			//rect.matcropped.copyTo(rect.matthd);
			findContours(rect.matthd, dieContours, dieHierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

			// Find and filter minimum area rects
			diceRectVec dotsRects;
			dotsRects.find_minAreaRect(dieContours, min, max, aspect_max);

			// Save dots count, and remember the colour for rectange
			if (dotsRects.size() >= 1 && dotsRects.size() <= 6)
			{
				size_t idx = dotsRects.size() - 1;
				occurrence.at(idx)++;
				rect.setDots(idx + 1);

				PRINT_DICE(rect);
			}
		}
	}

	return;
}

void diceRectVec::find_minAreaRect(vector<vector<Point>>& contours, int min, int max, float aspect_max)
{
	find_minAreaRect(contours, *this, min, max, aspect_max);
}

// draw contour to image
void diceRectVec::draw_minAreaRect(Mat & imageOut)
{
	for (diceRect &rect : *this)
	{
		// Draw square over  image
		Point2f corner_points[4];
		rect.points(corner_points);
		for (int j = 0; j < 4; j++)
		{
			const int line_width = 2 * imageOut.rows / 480;
			line(imageOut, corner_points[j], corner_points[(j + 1) % 4], rect.color, line_width, LINE_AA);
		}
	}
}

int diceRectVec::write_count(Mat & imageOut, int line)
{
	const char* fmt = "Num Dices: %d [%dx%d]";

	// prints number of found dices
	char buffer[32];
	sprintf(buffer, fmt, this->size(),imageOut.cols,imageOut.rows);
	line = write_text_line(imageOut, buffer, line);

	// prints dice occurrence to image
	fmt = "%d: %d";
	for (size_t i = 0; i < occurrence.size(); i++)
	{
		sprintf(buffer, fmt, i + 1, occurrence[i]);
		line = write_text_line(imageOut, buffer, line, diceRect::getDiceColor(i+1));
	}

	return line;
}

int diceRectVec::copy_dices2mat(cv::Mat & imageOut, cv::Size imgSize)
{
	if (this->empty())
		return -1;

	cv::Size maxSize;
	// find max dice Size
	for (const diceRect &rect : *this)
	{
		if (maxSize.height < rect.matthd.rows)
			maxSize.height = rect.matthd.rows;
		if (maxSize.width < rect.matthd.cols)
			maxSize.width = rect.matthd.cols;
	}

	imageOut = Mat::ones(imgSize, this->front().matthd.type())*255;

	const size_t MAX_COLS = imgSize.width / maxSize.width;
	const size_t MAX_ROWS = imgSize.height / maxSize.height;

	std::vector<cv::Rect> submat_rects;
	for (size_t n = 0; n < this->size(); n++)
	{
		const diceRect &drect = this->at(n);

		cv::Point_<int> ori;
		ori.x = (maxSize.width) * (n % MAX_COLS);
		ori.y = (maxSize.height) * (n / MAX_COLS % MAX_COLS);

		if (ori.y >= (int)MAX_ROWS)
			break;

		cv::Rect_<int> indx( ori, drect.matcropped.size());
		submat_rects.push_back(indx);

		if (drect.matcropped.size() != indx.size())
		{
			cout	<< "diceRectVec::copy_dices2mat(..): "
					<< "drect.matcropped.size() != indx.size()"
					<< drect.matcropped.size() << indx.size() << "\n";

			return -1;
		}
		drect.matcropped.copyTo(imageOut(indx));
	}


	if (imageOut.channels() < 3)
		cvtColor(imageOut, imageOut, cv::COLOR_GRAY2BGR);

	// draw grid cols:
	cv::Scalar color = diceRect::getDiceColor(1);
	const int line_width = 2 * imageOut.rows / 480;
	for (size_t n = 0; n < MAX_COLS; n++)
	{
		cv::Point_<int> p1(0,0);
		p1.x = (maxSize.width) * (n % MAX_COLS);

		cv::Point_<int> p2 = p1;
		p2.y = imageOut.rows;

		line(imageOut, p1, p2, color, line_width, LINE_AA);
	}
	// .. rows
	for (size_t n = 0; n < MAX_ROWS; n++)
	{
		cv::Point_<int> p1(0, 0);
		p1.y = (int)((maxSize.height) * (n % MAX_ROWS));

		cv::Point_<int> p2 = p1;
		p2.x = imageOut.cols;

		line(imageOut, p1, p2, color, line_width, LINE_AA);
	}


	return (int)submat_rects.size();
}

#if  defined __OPENCV_GUI
void diceRectVec::show_dice_images(const std::string &win0)
{
	//this->closeAllFigures();

	if (v_win.size() != this->size())
	{
		this->closeAllFigures();
		this->openAllFigures(win0);
	}

	for (size_t i = 0; i < std::min(v_win.size(),this->size()); i++)
	{
		imshow(v_win.at(i).c_str(), this->at(i).matthd);
	}

	//waitKey();
	//closeAllFigures();
}
#endif // defined __OPENCV_GUI

#if  defined __OPENCV_GUI
void diceRectVec::closeAllFigures()
{
	for (std::string &win : v_win)
		cvDestroyWindow(win.c_str());
}
#endif // defined __OPENCV_GUI

#if  defined __OPENCV_GUI
size_t diceRectVec::openAllFigures(const std::string &win0)
{
	v_win.clear();


	size_t n = 0;
	const int MAX_COLS = 10;
	for (diceRect &rect : *this)
	{
		int imshow_col = n % MAX_COLS ;
		int imshow_row = n / MAX_COLS % MAX_COLS;

		int pos_x = (10 + rect.matthd.cols) * imshow_col;
		int pos_y = (10 + rect.matthd.rows) * imshow_row;

		string _win = win0 + to_string(n++);
		namedWindow(_win);

		moveWindow(_win, pos_x, pos_y);

		v_win.push_back(_win);
	}

	return n;
}
#endif // defined __OPENCV_GUI























diceCounter::diceCounter()
{
	print_test = false;
	debug_prints = false;
}

diceCounter::~diceCounter()
{
}

int diceCounter::process_image(Mat * image, Mat * imageOut)
{
	Mat imageGray;
	int res = 0;
	if (image->channels() == 1)
	{
		imageGray = *image;
		PRINTMATPARA(imageGray);
		mv_time.start("process_image_pre(imageGray,..)");
		res = process_image_pre(imageGray, erosion_dst, *imageOut);
		mv_time.stop("process_image_pre(imageGray,..)");
	}
	else
	{
		PRINTMATPARA(*image);
		mv_time.start("process_image_pre_colour(..)");
		res = process_image_pre_colour(*image, erosion_dst, *imageOut);
		mv_time.stop("process_image_pre_colour(..)");
	}

	if (res == 0)
	{
		PRINTMATPARA(erosion_dst);
		mv_time.start("process_image_post(..)");
		res = process_image_post(*image, erosion_dst, *imageOut);
		mv_time.stop("process_image_post(..)");
	}

	PRINTMATPARA(*imageOut);
	return res;
}


void diceCounter::buildErosionKernel()
{
	int erosion_type = 0;

	if (erosion_elem == 0)
		erosion_type = MORPH_RECT;
	else if (erosion_elem == 1)
		erosion_type = MORPH_CROSS;
	else if (erosion_elem == 2)
		erosion_type = MORPH_ELLIPSE;

	ErosionElement = getStructuringElement(erosion_type,
		Size(2 * erosion_size + 1, 2 * erosion_size + 1),
		Point(erosion_size, erosion_size));
}
void diceCounter::buildDilationKernel()
{
	int dilation_type = 0;

	if (dilation_elem == 0)
		dilation_type = MORPH_RECT;
	else if (dilation_elem == 1)
		dilation_type = MORPH_CROSS;
	else if (dilation_elem == 2)
		dilation_type = MORPH_ELLIPSE;

	DilationElement = getStructuringElement(dilation_type,
		Size(2 * dilation_size + 1, 2 * dilation_size + 1),
		Point(dilation_size, dilation_size));
}


int diceCounter::process_image_pre(const Mat & imageGray, Mat & edge, Mat & imageOut)
{

	if (outputImgMode == 6) {
		cvtColor(imageGray, imageOut, cv::COLOR_GRAY2BGR);
		//canny_dst.copyTo(imageOut);
		return outputImgMode;
	}
	// 1. Preprocessing
	//cvtColor(*image, gray, COLOR_BGR2GRAY);

	int ksz = 0;
	ksz = 2 * mediansz + 1;
	if (medianmode == 0) {
		ksz = (mediansigma * 5) | 1; // gaussian
		GaussianBlur(imageGray, blurImage, Size(ksz, ksz), mediansigma, mediansigma);
	}
	else { //if (medianmode == 1)
		ksz = 2 * mediansz + 1;
		medianBlur(imageGray, blurImage, ksz);
	}

	if (outputImgMode == 1) {
		cvtColor(blurImage, imageOut, cv::COLOR_GRAY2BGR);
		//blurImage.copyTo(imageOut);
		return outputImgMode;
	}

	if (threshold_value)
		threshold(blurImage, thdimage, threshold_value, 255, THRESH_BINARY);
	else
		thdimage = blurImage;

	if (outputImgMode == 2) {
		cvtColor(thdimage, imageOut, cv::COLOR_GRAY2BGR);
		//thdimage.copyTo(imageOut);
		return outputImgMode;
	}

#if  defined NO_CANNY_EDGE
	static cv::Mat grad_x, grad_y;
	cv::Sobel(thdimage, grad_x, CV_16S, 1, 0);
	cv::Sobel(thdimage, grad_y, CV_16S, 0, 1);

	// converting back to CV_8U
	static cv::Mat abs_grad_x, abs_grad_y;
	cv::convertScaleAbs(grad_x, abs_grad_x);
	cv::convertScaleAbs(grad_y, abs_grad_y);

	cv::addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, canny_dst);
#else
	Canny(thdimage, canny_dst, edgeThresh, edgeThresh2, 3);
#endif // #if  defined NO_CANNY_EDGE

	//printf("edgeThresh: %d\n",edgeThresh);

	if (outputImgMode == 3) {
		cvtColor(canny_dst, imageOut, cv::COLOR_GRAY2BGR);
		//canny_dst.copyTo(imageOut);
		return outputImgMode;
	}

	if (dilation_mode == 4) // ero - dil
	{
		erode(canny_dst, erosion_dst, ErosionElement);
		dilate(erosion_dst, dilation_dst, DilationElement);
		edge = dilation_dst;
	}
	else if (dilation_mode == 3) // dil - ero
	{
		dilate(canny_dst, dilation_dst, ErosionElement);
		erode(dilation_dst, erosion_dst, ErosionElement);
		edge = erosion_dst;
	}
	else if (dilation_mode == 2) // dil
	{
		dilate(canny_dst, dilation_dst, DilationElement);
		edge = dilation_dst;
	}
	else if (dilation_mode == 1) // dil
	{
		erode(canny_dst, erosion_dst, ErosionElement);
		edge = erosion_dst;
	}
	else if (dilation_mode == 0)
		edge = canny_dst;

	if (outputImgMode == 4) {
		cvtColor(edge, imageOut, cv::COLOR_GRAY2BGR);
		//erosion_dst.copyTo(imageOut);
		return outputImgMode;
	}


	return 0;
}

int diceCounter::process_image_post(const Mat & im, const Mat & imageEdge, Mat & imageOut)
{
	vdices.clear();

	/* 2. Find Dice Contours
	findContours function:  transforms the previously
	detected edges into a list of contours, where each contour
	is a list of points that connected together form an edge.
	*/
	vector<vector<Point>> vcontours;
	vector<Vec4i> hierarchy;
	findContours(imageEdge, vcontours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	// For each contour, search the minimum area rectangle
	// using the minAreaRect function.
	float aspect_radio_derivation;
	aspect_radio_derivation = (float)con_dice_asp / 100.0;
	vdices.find_minAreaRect(vcontours, con_dice_min, con_dice_max, aspect_radio_derivation);

	// 3. Process each dice image
	aspect_radio_derivation = (float)con_dots_asp / 100.0;
	vdices.counting_dots(imageEdge, dotsThreshold, con_dots_min, con_dots_max, aspect_radio_derivation);

	if (outputImgMode == 5) {
		int res = vdices.copy_dices2mat(imageOut, imageEdge.size());
		if (res == 0)
			imageOut = cv::Mat::ones(imageEdge.size(), CV_8UC3)*255;
		return 1;
	}

	// 4. Draw frames and counts to colour image
	//*imageOut = image->clone();
	//cvtColor(erosion_dst, *imageOut, cv::COLOR_GRAY2BGR);
	if (im.channels() == 1)
		cvtColor(im, imageOut, cv::COLOR_GRAY2BGR);
	else
		im.copyTo(imageOut);

	// draw contour to image

	//draw_minAreaRect(imageOut, vdiceRects, vcolour);
	vdices.draw_minAreaRect(imageOut);
	// write number of found dices
	vdices.write_count(imageOut);

	// write duration time:
	if (print_test)
	{
		mv_time.stop(); // stop current measurement

		std::vector<std::string> _txt_duration;
		mv_time.get_duration(_txt_duration);
		int _line = 12;
		for (size_t i = 0; i < _txt_duration.size(); i++)
			_line = diceRectVec::write_text_line(imageOut, _txt_duration[i].c_str(), _line, Scalar(0, 0, 255), 0.4);
	}

	return 0;
}

int diceCounter::process_image_pre_colour(const Mat & im, Mat & erosion_dst, Mat & imageOut)
{
	Mat imageGray;
	cvtColor(im, imageGray, COLOR_BGR2GRAY);

	return process_image_pre( imageGray, erosion_dst, imageOut);

	/*
	int res = 0;

	static cv::Mat thdimage2, frame_HSV, edge1, edge2;

	// Convert from BGR to HSV colorspace
	int ksz = (3 * 5) | 1; // gaussian
	GaussianBlur(im, blurImage, Size(ksz, ksz), mediansigma, mediansigma);
	cvtColor(blurImage, frame_HSV, COLOR_BGR2HSV);


	// Detect the object based on HSV Range Values
	inRange(frame_HSV, get_thd_low(), get_thd_hight(), thdimage);


	if (dilation_mode == 4) // dil - ero - dil
	{
		dilate(thdimage, dilation_dst, DilationElement);
		if (erosion_size)
			erode(dilation_dst, edge1, ErosionElement);
		else
			edge1 = dilation_dst;
		dilate(edge1, edge2, DilationElement);
		edge1 = dilation_dst;
	}
	else if (dilation_mode == 3) // ero - dil - ero
	{
		erode(thdimage, edge1, ErosionElement);
		if (dilation_size)
			dilate(edge1, dilation_dst, DilationElement);
		else
			dilation_dst = edge1;
		erode(dilation_dst, edge1, ErosionElement);
	}
	else
	{
		dilate(thdimage, edge1, DilationElement);
	}

	Mat imageGray;
	cvtColor(im, imageGray, COLOR_BGR2GRAY);

	int _dilation_mode = dilation_mode;
	dilation_mode = 0; // dilation_mode off;
	res = process_image_pre(imageGray, canny_dst, imageOut);
	dilation_mode = _dilation_mode;


	if (dilation_mode == 0)
		canny_dst.copyTo(erosion_dst);
	else
		bitwise_and(edge1, canny_dst, erosion_dst);

	return res;
	//*/
}

para_dice::para_dice(int _blursz, int _thd, int _canny_thd, int _dots_thd, int _dilsz, int _dice_min, int _dice_max, float _dice_aratio, int _dot_min, int _dot_max, float _dot_aradio)
{
	outputImgMode = 0;

	medianmode = 1;
	mediansz = _blursz; //2;
	mediansigma = 5;

	threshold_value = _thd; // 146;
	edgeThresh = _canny_thd; //20;
	edgeThresh2= 3*_canny_thd;

	erosion_elem = 0;
	erosion_size = 0;
	//erosion_mode = 0;
	dilation_elem = 1;
	dilation_size = _dilsz; //2;
	dilation_mode = 2;

	con_dice_min = _dice_min; //3000;
	con_dice_max = _dice_max; // 5500;
	con_dice_asp = int(100.0*_dice_aratio); //25;

	con_dots_min = _dot_min; // 8;
	con_dots_max = _dot_max; // 150;
	con_dots_asp = int(100.0*_dot_aradio); // 60;

	dotsThreshold = _dots_thd;
}

para_dice::~para_dice()
{

}


void para_dice::scale_paramter(const float scale)
{
	mediansz = int(float(mediansz) * scale);
	mediansigma = int(float(mediansigma) * scale);

	//threshold_value = int(float(threshold_value) * scale); // 146;
	//edgeThresh = int(float(edgeThresh) * scale); //20;

	erosion_size = int(float(erosion_size) * scale);
	dilation_size = int(float(dilation_size) * scale); //2;

	con_dice_min = int(float(con_dice_min) * scale); //3000;
	con_dice_max = int(float(con_dice_max) * scale); // 5500;
													 //con_dice_asp = int(float(con_dice_asp) * scale); //25;

	con_dots_min = int(float(con_dots_min) * scale); // 8;
	con_dots_max = int(float(con_dots_max) * scale); // 150;
													 //con_dots_asp = int(float(con_dots_asp) * scale); // 60;

													 //dotsThreshold = int(float(dotsThreshold) * scale);

	this->buildErosionKernel();
	this->buildDilationKernel();
}
