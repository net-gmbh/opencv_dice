/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *
 */


#include "app.h"

#include "picProc.cpp"

#define __ConsoleOut
#define __SynviewLog

#if  defined ( __ConsoleOut ) && defined ( __SynviewLog )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}

#elif defined ( __ConsoleOut )
#  define PRINTF(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); }

#elif defined ( __SynviewLog )
#  define PRINTF(a) { LvLibrary::Logf a; }

#else
#  define PRINTF
#endif




// ----------------------------------------------------------------------------
//                 callback function for new frontend images
// ----------------------------------------------------------------------------

void LV_STDC StaticNewBufferCallbackFunction ( LvHBuffer hBuffer, void* pUserPointer, void* pUserParam)
{
    appClass* pApp = (appClass*) pUserParam;
    // in UserPointer we hold pointer to buffer
    pApp->NewBufferCallback ( (LvBuffer*) pUserPointer );
}


// -----------------------------------------------------------------------------
//                  callback function for gige server events
// -----------------------------------------------------------------------------

U32BIT StaticEventCallbackFunction ( U32BIT Event, U32BIT NrParam, U32BIT* Param, void* CBParam )
{
    appClass *pApp = (appClass *)CBParam;
    pApp->GigeEventCallback ( Event, NrParam, Param, CBParam );
    
    return 0;
}



// -----------------------------------------------------------------------------
//                          QR Decoder Application  
// -----------------------------------------------------------------------------

appClass::appClass (LvSystem* pSystem) {
    m_pSystem = pSystem;

    exitAppFlag=false;
    bIsConnected = false;
}

appClass::~appClass() {
}

bool appClass::ErrorOccurred(LvStatus ErrorStatus) {
    if (ErrorStatus == LVSTATUS_OK) return false;
    LvLibrary::Logf ( "Error:%s\n", LvLibrary::GetLastErrorMessage().c_str() );
    PRINTF (( "iamApp::synview error: \"%s\"", LvLibrary::GetLastErrorMessage().c_str() ));
    return true;
}


void appClass::calculateOutputImgSize() {
    if (sensorPixelFormat<0) PRINTF (( "ERROR 1208"));

    switch (sensorPixelFormat)
    {
    case LvPixelFormat_Mono8: // mono sensor --> decimate /1 and /2
        if (sensorDecimation==0) {
            imgWidthOut =int( sensorWidth/1);
            imgHeightOut=int( sensorHeight/1);
        } else {
            imgWidthOut =int( sensorWidth/2);
            imgHeightOut=int( sensorHeight/2);
        }
        break; //LvPixelFormat_BayerGR8
    case LvPixelFormat_BGR8: // RGB sensor --> decimate /1 and /2
        if (sensorDecimation==0) {
            imgWidthOut =int( sensorWidth/1);
            imgHeightOut=int( sensorHeight/1);
        } else {
            imgWidthOut =int( sensorWidth/2);
            imgHeightOut=int( sensorHeight/2);
        }
        break; //LvPixelFormat_BayerGR8
    default:    // bayer color sensor --> always decimate /2 during debayering
        if (sensorDecimation==0) {
            imgWidthOut =int( sensorWidth/2);
            imgHeightOut=int( sensorHeight/2);
        } else {
            imgWidthOut =int( sensorWidth/4);
            imgHeightOut=int( sensorHeight/4);
        }
        break;
    }

    PRINTF ((   "appClass::calculateOutputImgSize: sensorPixelFormat==LvPixelFormat_Mono8:" 
                "sensor: %dx%d imgOut: %dx%d", sensorWidth, sensorHeight, imgWidthOut, imgHeightOut));

}


// --- set camera frontend pixel format ---
void appClass::setSensorPixFmt() {    
       
    // in case of color sensor --> choose any available bayer format
    //sensorPixelFormat = LvPixelFormat_BayerGR8;
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);        
    
    if (SynviewStatus!=LVSTATUS_OK) {
        sensorPixelFormat = LvPixelFormat_BayerRG8;
        SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);

        if (SynviewStatus!=LVSTATUS_OK) {
            sensorPixelFormat = LvPixelFormat_BayerGB8;
            SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
            if (SynviewStatus!=LVSTATUS_OK) {
                sensorPixelFormat = LvPixelFormat_BayerBG8;
                SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);
                if (SynviewStatus!=LVSTATUS_OK) {
                    // monochrome sensor?
                    sensorPixelFormat = LvPixelFormat_Mono8;
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_PixelFormat, sensorPixelFormat);

                    if (SynviewStatus!=LVSTATUS_OK) {
                        PRINTF (( "Sensor Pixel format: No suitable pixel format found" ));        
                        sensorPixelFormat = -1;                
                    }                     
                }
            }
        }		
    }    
}

// --------------------------------------
//                 Init
// --------------------------------------

LvStatus appClass::init( )
{    
    exitAppFlag=false;

    // --- default settings ---
    sensorPixelFormat = LvPixelFormat_BGR8; //LvPixelFormat_BayerGR8;
    outputImageMode=0;  // 0 = RGB+overlay 1=mono input image
    sensorDecimation=0; // 0 = div1, 1=div2
    sensorDecimation_old = sensorDecimation;
    autoExposureMode=2; // 1 = use frontend ae 2=app mean 3=app max
    autoExposureTarget = 128;

    // --- open camera frontend ---
    printf("Open camera...\n");
    if (iamDevice.OpenCameraFrontend ( m_pSystem ) != LVSTATUS_OK)  
    {
        char Msg[1024];
        printf("Open camera failed\n");
        LvGetLastErrorMessage ( Msg, sizeof(Msg) );
        printf("Synview Error:%s\n", Msg);
        return (-1);
    }  

    // init the base camera class
    SynviewStatus = iamDevice.InitCameraFrontend();
    if (ErrorOccurred(SynviewStatus)) return SynviewStatus;
	
	   

	// --- camera frontend configuration ---  
	setSensorPixFmt();

    // use full sensor size
    SynviewStatus=iamDevice.GetInt32CameraParam( LvDevice_SensorWidth,  &sensorWidth );
    ErrorOccurred(SynviewStatus);
    SynviewStatus=iamDevice.GetInt32CameraParam( LvDevice_SensorHeight, &sensorHeight);
    ErrorOccurred(SynviewStatus);

    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_Width, sensorWidth);
    ErrorOccurred(SynviewStatus);
    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_Height, sensorHeight);
    ErrorOccurred(SynviewStatus);

    // get the current exposure value from frontend
    SynviewStatus = iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &exposureTime);
    ErrorOccurred(SynviewStatus);       
    
    // --- default settings ---

    if (outputImageMode==0 )outputPixelFormat=LvPixelFormat_BGR8;
    if (outputImageMode==1 )outputPixelFormat=LvPixelFormat_Mono8;
    
    calculateOutputImgSize();    
    
    // init dice detector:
    dice.scale_paramter(sensorWidth,sensorHeight);
    
    if (autoExposureMode==1) { // = use frontend ae
        SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Continuous);
        ErrorOccurred(SynviewStatus);    
    }

    SynviewStatus = iamDevice.SetFloatCameraParam ( LvDevice_Gamma, 100.0);
    ErrorOccurred(SynviewStatus);

    // --- init gige server ---
    //SynviewStatus = InitServer();

    // redirect new image callback
    iamDevice.SetCallback ( StaticNewBufferCallbackFunction, this );

    // enable Smart functions     
    iamDevice.SetInt32CameraParam  ( LvDevice_LvSmartGeneralInq,     7 );      // GlobalSmartEnable + GlobalFeatureLock + GlobalXMLEnable

    PRINTF (( "app:: Setting LvSmartAppXMLPath" ));
    iamDevice.m_pDevice->SetString ( LvDevice_LvSmartAppXMLPath, "./smartDices.xml");

    std::string path;
    iamDevice.m_pDevice->GetString ( LvDevice_LvSmartAppXMLPath, path);
    PRINTF (( "app:: LvSmartAppXMLPath has been set to: \"%s\"", path.c_str() ));

    // synview access to enable gige vision server
    int val;
    iamDevice.GetInt32CameraParam( LvDevice_LvGigEServerCapability, &val );
    if ( (val & 0x01) != 0 )
    {
        // enable gige server
        iamDevice.m_pDevice->SetInt ( LvDevice_LvGigEServerEnable, 0x01 );

        // check
        iamDevice.GetInt32CameraParam( LvDevice_LvGigEServerEnable, &val );
        PRINTF (( "appClass::InitServer: Enable GEV server. Result: %s", (val & 0x01) == 1?"OK":"NOT OK!" ));
        if ( (val & 0x01) == 1  )
        {            
            // register the event callback
            EventData Buffer;
            Buffer.pCBEvent   = (void *)StaticEventCallbackFunction;
            Buffer.pCBContext = (void *)this;

            PRINTF (( "appClass::InitServer: Register GigE Vision Server Event %p with context: %p", Buffer.pCBEvent, Buffer.pCBContext ));
            SynviewStatus = iamDevice.m_pDevice->SetBuffer ( LvDevice_LvGigEServerEvent, (void *)&Buffer, sizeof(Buffer) );
            ErrorOccurred(SynviewStatus);
        } else {
            PRINTF (( "Error: LvDevice_LvGigEServerEnable: 0" ));
            return (-1);
        }
    } else {
        PRINTF (( "Error: LvDevice_LvGigEServerCapability: none" ));
        return (-1);
    }   

    return LVSTATUS_OK;
}

void appClass::close() {
    iamDevice.CloseCameraFrontend();
}


		// --- GigE Server Event Processing ---
#define GEVSrvEv_Exit                   0x0000
#define GEVSrvEv_Connect                0x0001
#define GEVSrvEv_Disconnect             0x0002
#define GEVSrvEv_StreamStart            0x0003
#define GEVSrvEv_StreamStop             0x0004
#define GEVSrvEv_SmartXMLEvent          0x0010

#define GEVSrvEv_ReadFlag               0x0000
#define GEVSrvEv_WriteFlag              0x0001

#define GEVSrvEv_ImgFmt_DeviceWidth     0x0005
#define GEVSrvEv_ImgFmt_DeviceHeight    0x0006
#define GEVSrvEv_ImgFmt_PixFmt          0x0007

// --- XML Register Adresses ---
#define AdrSmartWidth             0x0100
#define AdrSmartHeight            0x0104
#define AdrSmartPixFmt            0x0200
#define AdrSmartPayloadSize       0x0300

#define AdrOutputImageFmt         0x0400
#define AdrSensorDecimation       0x0404
#define AdrExposureTime           0x0408
#define AdrAutoExposureMode       0x040C
#define AdrAutoExposureTarget     0x0410

#define AdrWhiteBalanceMode       0x0420

#define AdrOutputImageMode        0x0500
#define AdrMedianMode             0x0504
#define AdrMedianSz               0x0508
#define AdrMedianSigma            0x050C
#define AdrThreshold              0x0510
#define AdrEdgeThres              0x0514
#define AdrDilationMode           0x0518
#define AdrDilationElem           0x051C
#define AdrDilationSize           0x0520
#define AdrErosionElem            0x0524
#define AdrErosionSize            0x0528
#define AdrConDiceMin             0x052C
#define AdrConDiceMax             0x0530
#define AdrConDiceAsp             0x0534
#define AdrConDotsMin             0x0538
#define AdrConDotsMax             0x053C
#define AdrConDotsAsp             0x0540

#define AdrDotsThreshold          0x0544




// --------------------------------------------
//         GigE Server Event Callback 
// --------------------------------------------
void appClass::GigeEventCallback ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam )
{
    int error=0;
    int Write = int(Param[0]);
        
    // --- GigE Server Event: Connect ---
    if ( Event == GEVSrvEv_Connect ) {
        PRINTF(("GigE Server Event: Connect"));
        if ( bIsConnected ) {
            PRINTF(("ERROR: Already connected!"));
        }
        bIsConnected = true;
    }

    
    // --- GigE Server Event: Disconnect ---
    else if ( Event == GEVSrvEv_Disconnect )    {
        PRINTF(("GigE Server Event: Start Disconnect..."));
        if ( !bIsConnected ) {
            PRINTF(("ERROR: Not connected!"));
        }

        if ( iamDevice.IsAcquiring () ) {
            PRINTF(("Stopping acquisition"));
            iamDevice.StopAcquisition ();

            PRINTF (("Closing buffers" ));
            iamDevice.CloseBuffers ();
        }

        bIsConnected = false;
        PRINTF(("Disconnect done"));
    }

    // --- GigE Server Event: Exit request ---
    else if ( Event == GEVSrvEv_Exit ) {
        PRINTF(("GigE Server Event: Exit..."));
        int *data  = (int*) &Param[2];

        if ( *data == 0 ) {     // just exit
            exitAppFlag = true;
            bIsConnected = false;
        }
    }

    // --- GigE Server Event: Stream Start (=GigE acquisition start) ---
    else if ( Event == GEVSrvEv_StreamStart ) {
        int *data = (int *) &Param[2];

        if ( Write ) {
            PRINTF(("GigE Server Event: Start Stream Start..."));
            if ( iamDevice.IsAcquiring () ) {
                PRINTF(("Stopping camera"));
                iamDevice.StopAcquisition ();

                PRINTF (( "Closing buffers" ));
                iamDevice.CloseBuffers();
            }

            error = iamDevice.OpenBuffers ();
            if ( error ) {
                PRINTF(("ERROR: OpenBuffers error"));
            }

            // start frontend acquisition
            error = iamDevice.StartAcquisition ();
            if ( error ) {
                PRINTF(("ERROR: StopAcquisition error"));
            }

            PRINTF(("Start stream done"));
        } else {
            *data = iamDevice.IsAcquiring () ? 0 : 1;     // reading the register returns the command value (1) until the acquisition is started (0) 
            PRINTF(("GigE Server Event: GEVSrvEv_StreamStart read value:%d", *data ));
        }
    }
    
    // --- GigE Server Event: Stream Stop (=GigE acquisition stop) ---
    else if ( Event == GEVSrvEv_StreamStop )
    {
        int *data = (int *) &Param[2];

        if ( Write ) {
            PRINTF(("GigE Server Event: Start Stream Stop..."));
            error = iamDevice.StopAcquisition ();
            if ( error) {
                PRINTF(("ERROR: StopAcquisition error"));
            }

            PRINTF(("Closing buffers"));
            iamDevice.CloseBuffers ();

            PRINTF(("Stop stream done"));
        } else {
            *data = iamDevice.IsAcquiring () ? 1 : 0;    // reading the register returns the command value (1) until the acquisition is stopped (0)
            PRINTF(("GigE Server Event: GEVSrvEv_StreamStop read value:%d", *data ));
        }
    }
    
    // --- GigE Server Event: XML Parameter Read/Write ---
    else if (Event == GEVSrvEv_SmartXMLEvent ) {
        unsigned int adr   = (unsigned int)  Param[1];
        int         *data  = (int        *) &Param[2];
        float       *fdata = (float*)       &Param[2];

        if ( adr == AdrSmartWidth ) // width of GigE-Server output image
        {
            if ( Write ) {
                PRINTF (( "XmlEvent: Write(AdrSmartWidth) --> ERROR !" ));
                // read only in XML
            }
            else {
                *data=imgWidthOut;
                PRINTF (( "XmlEvent: Read(AdrSmartWidth) -->: %d", *data ));                
            }

        }

        else if ( adr == AdrSmartHeight ) // height of GigE-Server output image
        {
            if ( Write ) {
                PRINTF (( "XmlEvent: Write(AdrSmartHeight) --> ERROR" ));
                // read only in XML
            }
            else {
                *data=imgHeightOut;
                PRINTF (( "XmlEvent: Read(AdrSmartHeight) --> %d", *data ));
            }
        }

        else if ( adr == AdrSmartPixFmt ) // pixel format of GigE-Server output image
        {
            if ( Write ) {
                PRINTF (( "XmlEvent: Write(AdrSmartPixFmt) -->  0x%8.8x -->ERROR", *data ));
                // read only in XML       
            }
            else {
                *data=outputPixelFormat; 
                PRINTF (( "XmlEvent: Read(AdrSmartPixFmt) --> 0x%8.8x", *data ));
            }
        }

        else if ( adr == AdrSmartPayloadSize )
        {
            if ( Write ) {
                // read only in XML
                PRINTF (( "XmlEvent: Write(AdrSmartPayloadSize) --> ERROR" ));
            }
            else {                
                int SmartPayloadSize = imgWidthOut * imgHeightOut;
                if (outputPixelFormat==LvPixelFormat_BGR8) SmartPayloadSize *=3;
                PRINTF (( "iAMServerClass::ClassEventCallback: GEVSrvEv_SmartXMLEvent Read PayloadSize:" 
                            "wxh: %dx%d bpp:%d (%d) SmartPayloadSize:%d", 
                            imgWidthOut, imgHeightOut, 3, outputPixelFormat , SmartPayloadSize ));// XML invalitated from width, height and pixel format
                *data = SmartPayloadSize;
                PRINTF (( "XmlEvent: Read(AdrSmartPayloadSize) --> PayloadSize: %d Byte", *data ));
            }
        }

        else if ( adr == AdrOutputImageFmt )
        {
            if ( Write ) {
                outputImageMode = *data;
                PRINTF (( "XmlEvent: Write(AdrOutputFormat) --> %d", *data ));
                // 0=RGB+overlay 1=mono+embedded
                if (outputImageMode==0 ) outputPixelFormat=LvPixelFormat_BGR8;
                else if (outputImageMode==5 ) outputPixelFormat=LvPixelFormat_BGR8;
                //else outputPixelFormat=LvPixelFormat_Mono8;
                outputPixelFormat=LvPixelFormat_BGR8;
                // --> xml invalidates pixel format
            }
            else {
                *data=outputImageMode;
                PRINTF (( "XmlEvent: Read(AdrOutputFormat) -->: %d", *data ));
            }
        }

        else if ( adr == AdrSensorDecimation )
        {
            if ( Write ) {
                PRINTF (( "XmlEvent: Write(AdrSensorDecimation) --> %d", *data ));
                // xml enum: 0=div1, 1=div2
                sensorDecimation=*data;
                if (sensorDecimation>1) sensorDecimation=1;
                if (sensorDecimation<0) sensorDecimation=0;	    
                calculateOutputImgSize();   

                if (sensorDecimation == 0 && sensorDecimation_old == 1)
                    dice.scale_paramter(2);
                if (sensorDecimation == 1 && sensorDecimation_old == 0)
                    dice.scale_paramter(0.5);
                sensorDecimation_old = sensorDecimation;

                 
                PRINTF ((   "appClass::calculateOutputImgSize: dice.scale_paramter(imgWidthOut,imgHeightOut):" 
                "scale_paramter(%d,%d), imgOut: %dx%d", sensorWidth, sensorHeight, imgWidthOut, imgHeightOut)); 
                // --> xml invalidates width and height     
            }
            else {
                *data=sensorDecimation;
                PRINTF (( "XmlEvent: Read(AdrSensorDecimation) -->: %d", *data ));
            }
        }

        else if ( adr == AdrExposureTime )
        {
            if ( Write ) {
                exposureTime = *fdata;
                iamDevice.SetFloatCameraParam(LvDevice_ExposureTime,  exposureTime);
                iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &exposureTime);
                PRINTF (( "XmlEvent: Write(AdrExposureTime) --> %f (%f)", *fdata,exposureTime ));                
            }
            else {
                *fdata = exposureTime;
                PRINTF (( "XmlEvent: Read(AdrExposureTime) -->: %f", exposureTime ));
            }
        }

        else if ( adr == AdrAutoExposureMode )
        {
            if ( Write ) {
                autoExposureMode = *data;
                PRINTF (( "XmlEvent: Write(AdrAutoExposureMode) --> %d", *data ));   
                if (autoExposureMode==0) {  //Off
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(SynviewStatus);  
                }           
                if (autoExposureMode==1) {  // Fronten AE
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Continuous);
                    ErrorOccurred(SynviewStatus);  
                }    
                if (autoExposureMode==2) { // App AE on Mean
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(SynviewStatus);  
                }     
                if (autoExposureMode==3) { // App AE on Max
                    SynviewStatus = iamDevice.SetInt32CameraParam ( LvDevice_ExposureAuto, LvExposureAuto_Off);
                    ErrorOccurred(SynviewStatus);  
                }  
                // get current exposure value from frontend
                SynviewStatus = iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &exposureTime);
                ErrorOccurred(SynviewStatus);           
            }
            else {
                *data = autoExposureMode;
                PRINTF (( "XmlEvent: Read(AdrAutoExposureMode) -->: %d", *data ));
            }
        }

        
        else if ( adr == AdrAutoExposureTarget ) {
            if ( Write ) {
                autoExposureTarget = *data;                
            }
            else {
                *data=autoExposureTarget;                
            }
        }


        else if ( adr == AdrOutputImageMode ) {
            if ( Write ) {
                dice.outputImgMode = *data;                
            }
            else {                                                                                                                                               
                *data=dice.outputImgMode;                
            }
        }

        else if ( adr == AdrMedianMode ) {
            if ( Write ) {
                dice.medianmode = *data;                
            }
            else {
                *data=dice.medianmode;                
            }
        }

        else if ( adr == AdrMedianSz ) {
            if ( Write ) {
                dice.mediansz = *data;                
            }
            else {
                *data=dice.mediansz;                
            }
        }

        else if ( adr == AdrMedianSigma ) {
            if ( Write ) {
                dice.mediansigma = *data;                
            }
            else {
                *data=dice.mediansigma;                
            }
        }

        else if ( adr == AdrThreshold ) {
            if ( Write ) {
                dice.threshold_value = *data;                
            }
            else {
                *data=dice.threshold_value;                
            }
        }

        else if ( adr == AdrEdgeThres ) {
            if ( Write ) {
                dice.edgeThresh = *data;   
                dice.edgeThresh2=3*(*data);             
            }
            else {
                *data=dice.edgeThresh;                
            }
        }

        else if ( adr == AdrDilationMode ) {
            if ( Write ) {
                dice.dilation_mode = *data;  
                dice.buildErosionKernel ();
                dice.buildDilationKernel();              
            }
            else {
                *data=dice.dilation_mode;                
            }
        }

        else if ( adr == AdrDilationElem ) {
            if ( Write ) {
                dice.dilation_elem = *data;   
                dice.buildDilationKernel();                
            }
            else {
                *data=dice.dilation_elem;                
            }
        }

        else if ( adr == AdrDilationSize ) {
            if ( Write ) {
                dice.dilation_size = *data;   
                dice.buildDilationKernel();             
            }
            else {
                *data=dice.dilation_size;                
            }
        }

        else if ( adr == AdrErosionElem ) {
            if ( Write ) {
                dice.erosion_elem = *data;   
                dice.buildErosionKernel ();             
            }
            else {
                *data=dice.erosion_elem;                
            }
        }

        else if ( adr == AdrErosionSize ) {
            if ( Write ) {
                dice.erosion_size = *data;   
                dice.buildErosionKernel ();             
            }
            else {
                *data=dice.erosion_size;                
            }
        }

        else if ( adr == AdrConDiceMin ) {
            if ( Write ) {
                dice.con_dice_min = *data;                
            }
            else {
                *data=dice.con_dice_min;                
            }
        }

        else if ( adr == AdrConDiceMax ) {
            if ( Write ) {
                dice.con_dice_max = *data;                
            }
            else {
                *data=dice.con_dice_max;                
            }
        }

        else if ( adr == AdrConDiceAsp ) {
            if ( Write ) {
                dice.con_dice_asp = *data;                
            }
            else {
                *data=dice.con_dice_asp;                
            }
        }

        else if ( adr == AdrConDotsMin ) {
            if ( Write ) {
                dice.con_dots_min = *data;                
            }
            else {
                *data=dice.con_dots_min;                
            }
        }

        else if ( adr == AdrConDotsMax ) {
            if ( Write ) {
                dice.con_dots_max = *data;                
            }
            else {
                *data=dice.con_dots_max;                
            }
        }

        else if ( adr == AdrConDotsAsp ) {
            if ( Write ) {
                dice.con_dots_asp = *data;                
            }
            else {
                *data=dice.con_dots_asp;                
            }
        }

        else if ( adr == AdrDotsThreshold ) {
            if ( Write ) {
                dice.dotsThreshold = *data;                
            }
            else {
                *data=dice.dotsThreshold;                
            }
        }


        else if ( adr == AdrWhiteBalanceMode )
        {
            // Create a map of three strings (that map to integers)
            static std::map<std::string, int> map_str2val { {"Off", 0}, {"Once", 1}, {"Continuous", 2}, };
            static std::map<int, std::string> map_val2str { {0, "Off"}, {1, "Once"}, {2, "Continuous"}, };

            auto pData = data;
            iamDeviceClass* iamDevice = &(this->iamDevice);

            if ( Write ) {
                int val = getInt ( pData );
                LvFeature Ftr_WhiteBalanceAuto;
                iamDevice->m_pDevice->GetFeatureByName(LvFtrGroup_DeviceRemote, "WhiteBalanceAuto", &Ftr_WhiteBalanceAuto);

                if (iamDevice->m_pDevice->SetEnumStr(Ftr_WhiteBalanceAuto, map_val2str.find(val)->second.c_str())!= LVSTATUS_OK ) {
                    PRINTF (( "ERROR: iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrWhiteBalanceMode:  %d %s\n", val, map_val2str.find(val)->second.c_str() ));
                }
                else {
                    PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Write AdrWhiteBalanceMode: %d %s", val, map_val2str.find(val)->second.c_str() ));
                }
            }
            else {
                LvFeature Ftr_WhiteBalanceAuto;
                char Val_WhiteBalanceAuto[256];
                iamDevice->m_pDevice->GetFeatureByName(LvFtrGroup_DeviceRemote, "WhiteBalanceAuto", &Ftr_WhiteBalanceAuto);
                if (iamDevice->m_pDevice->GetEnumStr(Ftr_WhiteBalanceAuto, Val_WhiteBalanceAuto, sizeof(Val_WhiteBalanceAuto))!= LVSTATUS_OK ) {
                    PRINTF (( "ERROR: iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrWhiteBalanceMode: %d %s\n", map_str2val.find(Val_WhiteBalanceAuto)->second, Val_WhiteBalanceAuto ));
                }
                else {
                    putInt ( pData, map_str2val.find(Val_WhiteBalanceAuto)->second );
                    PRINTF (( "iAMServerClass::ServerEventCallback: GEVSrvEv_SmartXMLEvent Read AdrWhiteBalanceMode: %d %s", map_str2val.find(Val_WhiteBalanceAuto)->second, Val_WhiteBalanceAuto ));
                }
            }
        }





    }

    return;
}





// ----------------------------------------------------------------------------
//                     Callback for new frontend image
// ----------------------------------------------------------------------------


void appClass::NewBufferCallback(LvBuffer* pBuffer)
{
    //PRINTF (( "appClass::CallbackNewBuffer [begin] (Buffer:%p)", pBuffer ));
    int AwaitDelivery;
    int NumUnderrunAct;

    // --- buffer undefined? ---
    if ( pBuffer == 0 )
    {
        PRINTF (( "CallbackNewBuffer: buffer pointer undefined [end]" ));
        return;
    }
    // --- event no longer defined? ---
    if ( iamDevice.m_pEvent == NULL )
    {
        PRINTF (( "CallbackNewBuffer: event not defined! [end]" ));
        pBuffer->Queue();
        return;
    }

    // --- close event requested? ---
    if ( iamDevice.m_pEvent->CallbackMustExit () )
    {
        PRINTF (( "CallbackNewBuffer: exit requested! [end]" ));
        pBuffer->Queue();
        return;
    }

    // --- get buffer pool info ---
    if (iamDevice.m_pStream->GetInt32 ( LvStream_LvNumAwaitDelivery, &AwaitDelivery) != LVSTATUS_OK ) {
        PRINTF (( "appClass::ivQueryFrameGenTL: error get AwaitDelivery" ));
    }
    if (iamDevice.m_pStream->GetInt32 ( LvStream_LvNumUnderrun, &NumUnderrunAct) != LVSTATUS_OK ) {
        PRINTF (( "appClass::ivQueryFrameGenTL: error get NumUnderrun" ));
    }
    //PRINTF (( "appClass::ivQueryFrameGenTL: buffers in aquisition:%d underrun frames:%d aquiring flag:%d", AwaitDelivery, NumUnderrunAct-NumUnderrunLast, IsAcquiring() ));
        
    
    // --- get the image ---   

    LvipImgInfo ImgInfo;
    int err = pBuffer->GetImgInfo ( ImgInfo );       // depends on Uni Processing mode: if "Off" or "HwOnly", this returns the unprocessed buffer info
    if (err) {
        PRINTF (( "ERROR: CallbackNewBuffer::ivQueryFrameGenTL: Cannot retrieve image info" ));
        pBuffer->Queue();
        return;
    }
    
    uint8_t* pImageData = (uint8_t*)ImgInfo.pData; // get image data pointer
    int ImgWidth         = ImgInfo.Width;
    int ImgHeight        = ImgInfo.Height;
    //int ImgLinePitch     = ImgInfo.LinePitch;
    int ImgBytesPerPixel = ImgInfo.BytesPerPixel;
    int ImgPixelFormat   = ImgInfo.PixelFormat;
	
	auto lpxf2str = [](int _pxf){ 	
									if (_pxf == LvPixelFormat_BGR8) return "LvPixelFormat_BGR8";
									if (_pxf == LvPixelFormat_Mono8) return "LvPixelFormat_Mono8";
									return "other"; };

    //PRINTF (( "CallbackNewBuffer..."));
    PRINTF (( "  Input  img: Width:%d Height:%d BPP:%d PixFmt: %s pData:%p", ImgWidth, ImgHeight, ImgBytesPerPixel, lpxf2str(ImgPixelFormat), pImageData ));
	/*
    if (ImgBytesPerPixel!=1) { // mono/bayer from frontend only
        PRINTF (( "Sensor pixel format not supported (1202) !!"));
        pBuffer->Queue();
        return;
    } // */   

    // -----------------------------
    // --- Preprocessing         ---
    // -----------------------------

    cv::Mat inPicMono;	
    cv::Mat inPicMat;

    // mono sensor                --> take sensor image or scale down by 2
    // color sensor in bayer mode --> fast bayer to mono including reduction by 2 or 4
    if (ImgPixelFormat == LvPixelFormat_BGR8) { //BGR8
        if (sensorDecimation ==0){
            inPicMat = cv::Mat(cv::Size(ImgWidth, ImgHeight), CV_8UC3,(unsigned char*) pImageData); // use sensor data directly
            cvtColor(inPicMat, inPicMono, cv::COLOR_BGR2GRAY);         
        } else {
            inPicMat = cv::Mat(cv::Size(ImgWidth, ImgHeight), CV_8UC3,(unsigned char*) pImageData); // use sensor data directly
            cv::Mat imgray;
            cvtColor(inPicMat, imgray, cv::COLOR_BGR2GRAY);            
            cv::resize(imgray,inPicMono,cv::Size (ImgWidth/2,ImgHeight/2),cv::INTER_AREA  );           // scale down
            PRINTF (( "  (ImgPixelFormat == LvPixelFormat_BGR8): Resize  img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", 
                        inPicMono.cols, inPicMono.rows, (int)inPicMono.elemSize(), inPicMono.type(), inPicMono.data ));
        }

    }
    else if (ImgPixelFormat==LvPixelFormat_Mono8) { // mono sensor
        if (sensorDecimation==0) {
            inPicMono = cv::Mat(cv::Size(ImgWidth, ImgHeight), CV_8U,(unsigned char*) pImageData); // use sensor data directly            
        } else {
            inPicMat = cv::Mat(cv::Size(ImgWidth, ImgHeight), CV_8U,(unsigned char*) pImageData);            
            cv::resize(inPicMat,inPicMono,cv::Size (ImgWidth/2,ImgHeight/2),cv::INTER_AREA  );           // scale down
            PRINTF (( "  (ImgPixelFormat==LvPixelFormat_Mono8): Resize  img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", 
                        inPicMono.cols, inPicMono.rows, (int)inPicMono.elemSize(), inPicMono.type(), inPicMono.data ));
        }
    } else {	
        if (sensorDecimation==0) {            // color sensor in bayer mode
            inPicMono = cv::Mat(cv::Size(ImgWidth/2, ImgHeight/2), CV_8U);
            bayerFade2GrayDecimate2((uint8_t *) pImageData, inPicMono.ptr(),ImgWidth, ImgHeight);  // bayer-->mono + div2 pixel reduction 
        } else {
            inPicMono = cv::Mat(cv::Size(ImgWidth/4, ImgHeight/4), CV_8U);
            bayerFade2GrayDecimate4((uint8_t *) pImageData, inPicMono.ptr(),ImgWidth, ImgHeight);  // bayer-->mono + div4 pixel reduction 
        }
    }
	
    // -----------------------------
    // --- Dice Detect           ---
    // -----------------------------
    cv::Mat outPic;
    int ok = dice.process_image(&inPicMono, &outPic);
    PRINTF (( "  dice.process_image(&inPicMono, &outPic): exits with %d", ok));

    // time measure evaluation
	dice.mv_time.print_duration();

    // -------------------------------------------------
    // --- Auto Exposure Control on Image Mean value ---
    // -------------------------------------------------
	if (autoExposureMode==2) {
        double ev;
        float newExpValue;

        cv::Scalar mv = cv::mean(inPicMono);        
        ev=mv.val[0];  
        int evDelta = int(ev)-autoExposureTarget;

        iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &newExpValue);                                  
        if ( evDelta>0) { // too bright
            if      ( evDelta>92) newExpValue/=1.3;
            else if ( evDelta>52) newExpValue/=1.15;
            else if ( evDelta>22) newExpValue/=1.06;            
        } else {        // too dark
            if      ( evDelta< -98) newExpValue*=1.3;
            else if ( evDelta< -78) newExpValue*=1.15;
            else if ( evDelta< -28) newExpValue*=1.06;
        }
        if (newExpValue<   100) newExpValue=100;    // 100 us
        if (newExpValue>250000) newExpValue=250000; // 0.25 sec
        
        exposureTime=newExpValue;
        iamDevice.SetFloatCameraParam(LvDevice_ExposureTime,  newExpValue);
        PRINTF (( "AE: EV: %f --> Exposure: %f",ev,newExpValue ));         
	}

    // -------------------------------------------------
    // --- Auto Exposure Control on Image Max value  ---
    // -------------------------------------------------
	if (autoExposureMode==3) {
        //double ev;
        float newExpValue;

        double imgMin, imgMax;
        cv::minMaxLoc(2, &imgMin, &imgMax);

        /*cv::Scalar mv = cv::mean(inPicMono);        
        ev=mv.val[0];  
        int evDelta = int(ev)-autoExposureTarget;

        iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &newExpValue);                                  
        if ( evDelta>0) { // too bright
            if      ( evDelta>92) newExpValue/=1.3;
            else if ( evDelta>52) newExpValue/=1.15;
            else if ( evDelta>22) newExpValue/=1.06;            
        } else {        // too dark
            if      ( evDelta< -98) newExpValue*=1.3;
            else if ( evDelta< -78) newExpValue*=1.15;
            else if ( evDelta< -28) newExpValue*=1.06;
        }*/


        iamDevice.GetFloatCameraParam(LvDevice_ExposureTime, &newExpValue); 
        if      (imgMax>245)  newExpValue/=1.2;
        else if (imgMax>230)  newExpValue/=1.06;
        else if (imgMax<80)  newExpValue*=1.3;
        else if (imgMax<120)  newExpValue*=1.15;
        else if (imgMax<200)  newExpValue*=1.06;


        if (newExpValue<   100) newExpValue=100;    // 100 us
        if (newExpValue>250000) newExpValue=250000; // 0.25 sec
        
        exposureTime=newExpValue;
        iamDevice.SetFloatCameraParam(LvDevice_ExposureTime,  newExpValue);
        PRINTF (( "AE: Max: %f --> Exposure: %f",imgMax,newExpValue ));         
	}

    // -----------------------------
    // --- Generate Output Image  ---
    // -----------------------------
  
        ImgInfo.Width=outPic.cols;
        ImgInfo.Height=outPic.rows;
        ImgInfo.LinePitch=outPic.cols * outPic.elemSize();
        ImgInfo.PixelFormat=outputPixelFormat;
        ImgInfo.BytesPerPixel=outPic.elemSize();
        ImgInfo.pData=outPic.ptr();

        // check buffer

        //#  define CHECK(a) { printf("[%05d]:", OsGetTickCount()%100000); printf a; printf("\n"); LvLibrary::Logf a;}
        #  define CHECK(a) { \
            if ( a )  { \
                PRINTF (( "appClass::NewBufferCallback: Output Buffer format is not supported (%s)", #a)); \
                pBuffer->Queue();  return; } }

       
        CHECK ((int)ImgInfo.Width != imgWidthOut)
        CHECK ((int)ImgInfo.Height != imgHeightOut)
        if (outputPixelFormat == LvPixelFormat_Mono8)
            CHECK (ImgInfo.BytesPerPixel != 1)

        else 
            CHECK (ImgInfo.BytesPerPixel != 3)
        


        PRINTF (( "  Output img: Width:%d Height:%d BPP:%d PixFmt:%d pData:%p", ImgInfo.Width, ImgInfo.Height, ImgInfo.BytesPerPixel, ImgInfo.PixelFormat, ImgInfo.pData ));
	    
        SendImage ( &ImgInfo ); // send image to gigeserver    
        pBuffer->Queue();       // put buffer back into pool

    return;
}


LvStatus appClass::SendImage ( LvipImgInfo* ImgInfo )
{
    //PRINTF (( "iAMServerClass::SendImage: LvDevice_LvGigEServerImage: pImgInfo:%p pImg:%p PixFmt:%x Width:%d Height:%d LinePitch:%d", (int64_t)ImgInfo, (int64_t)ImgInfo->pData, ImgInfo->PixelFormat, ImgInfo->Width, ImgInfo->Height, ImgInfo->LinePitch ));
    LvStatus RetVal = iamDevice.m_pDevice->SetInt ( LvDevice_LvGigEServerImage, (int64_t)ImgInfo );
    return RetVal;
}
