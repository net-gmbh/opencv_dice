/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *	b.hoffmann - 26.03.2020
 */

#ifndef AppClass_H
#define AppClass_H

#include <map>

#include "OsDep.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/opencv.hpp>

#include "iamDevice.h"


#include "dice.h"


class appClass 
{
public:
    appClass(LvSystem* pSystem);
    ~appClass();

    LvSystem*    m_pSystem;

    iamDeviceClass iamDevice;

    LvStatus init();    
    void     close();
    
    void     GigeEventCallback  ( U32BIT Event, U32BIT NrParam, U32BIT *Param, void *CBParam );    
    void     NewBufferCallback  ( LvBuffer* pBuffer );

    void calculateOutputImgSize();
    void setSensorPixFmt();

    LvStatus SynviewStatus;    
    bool ErrorOccurred          ( LvStatus ErrorStatus);

    LvStatus SendImage          ( LvipImgInfo* pImgInfo );
   	
    // --- QR decoder --- 
    

    diceCounter dice;
    
    
    int   sensorWidth;
    int   sensorHeight;

    int   imgWidthOut;
    int   imgHeightOut;
    int   sensorPixelFormat;

    int   sensorDecimation, sensorDecimation_old;
    int   outputImageMode; 
    int   outputPixelFormat;
    int   autoExposureMode;
    int   autoExposureTarget;
    float exposureTime;

    bool  exitAppFlag;
    bool  bIsConnected;

    typedef struct _EventData
    {
        void*  pCBEvent;
        void*  pCBContext;
    } EventData;
    
};

// interface functions
inline int   getInt     ( void* pData )               { return *(int *) pData; }
inline void  putInt     ( void* pData, int Data )     { *(int *) pData = Data; }
inline float getFloat   ( void* pData )               { return *(float *) pData; }
inline void  putFloat   ( void* pData, float Data )   { *(float *) pData = Data; }
inline char* getCharPtr ( void* pData )               { return (char *)( ((int *)pData)[0] + (((U64BIT)((int *)pData)[1])<<32) );}
inline void  putCharPtr ( void* pData, char *p )      { ((int *)pData)[0] = (U32BIT)((U64BIT)p)&0xffffffff; ((int *)pData)[1] = (U32BIT)(((U64BIT)p)>>32); }

#endif //AppClass_H
