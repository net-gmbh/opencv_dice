/*-----------------------------------------------------------------------------
 *                                                   ______________
 *                                     _            / _____________ \
 *                                    | |          / /       ____  \ \
 *                                    | |         / /       |___ \  \ \
 *                                    | |        / /       ___  \ \  \ \
 *            ________     ________   | |____   /_/  __   /   \  \ \  \ \
 *           / ______ \   / ______ \  |  ____|       \ \  \___/  / /  / /
 *          | |      | | | |  ____| | | |             \ \_______/ /  / /
 *          | |      | | | | |_____/  | |              \_________/  / /
 *          | |      | | | |________  | |________          ________/ /
 *          |_|      |_|  \_________|  \_________|        |_________/
 *
 *----------------------------------------------------------------------------
 * Copyright 2020 NEW ELECTRONIC TECHNOLOGY GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 *  2020 NET GmbH,  Lerchenberg 7, 86923 Finning.
 *	b.hoffmann - 26.03.2020
 */

#ifndef CV_TIME_MEASURE
#define CV_TIME_MEASURE

//#include <execution>
#include <vector>
#include <deque>
#include <map>
#include "opencv2/core/utility.hpp"

#define CONTAINER deque
#define MOV_AVG_SZ 1000

struct cv_time
{
	int64 t1, t2;
	double d, dsum;
	cv_time(double last_duration = 0) : t1(1), t2(0), d(-1) { dsum = last_duration; }

	inline void start()		{	t1 = cv::getTickCount();	}
	inline double stop()	{	t2 = cv::getTickCount();
								d = double(t2 - t1) / cv::getTickFrequency(); 
								return d; }
	inline double time() { if (!t2) abort(); return d; }
};


class cv_time_vec : public std::CONTAINER<cv_time>
{
public:
	cv_time_vec(std::string _sectionname = "dummy") : sectionname(_sectionname) {}
	~cv_time_vec() {}

	const std::string sectionname;

	inline void start(const std::string &id)
	{
		if (id.compare(sectionname) != 0)
			std::abort();
		
		// check if last element was stoped:
		if (this->size() && !this->front().t2) 
			std::abort();
		this->push_back(cv_time());
		this->back().start();
	}

	inline double stop()
	{
		return this->back().stop();
	}

	inline double sum()
	{
		while (this->size() > MOV_AVG_SZ)
		{
			this->pop_front();
		}
		
		double _sum = 0.0;
		for (auto &t : *this)
			_sum += t.time();

		return _sum;
	}
	inline double mean()
	{
		return this->sum() / (double)this->size();
	}

private:

};





class cv_time_class : public std::map<const std::string, cv_time_vec>
{
public:
	cv_time_class() {}
	~cv_time_class() {}

	inline void start(const std::string &id)
	{
		if (this->find(id) == this->end())
			if (this->insert(std::make_pair(id, cv_time_vec(id))).second == false)
				std::abort();

		m_current_id = id;
		(*this)[id].start(id);
	}

	inline double stop(const std::string &id = "")
	{
		const std::string *pID = &id;
		if (id.empty() && !m_current_id.empty())
			pID = &m_current_id;
		if (this->find(*pID) == this->end())
			std::abort();

		double res = (*this)[*pID].stop();
		m_current_id.clear();
		
		return res;
	}

	inline double mean(const std::string &id)
	{
		if (this->find(id) == this->end())
			std::abort();
		return (*this)[id].mean();
	}

	inline double get_duration()
	{
		double sum(0.0);
		// Iterate through all elements in std::map
		std::map<const std::string, cv_time_vec>::iterator it = this->begin();
		while (it != this->end())
		{
			sum += it->second.mean();
			it++;
		}

		return sum;
	}

	inline  void get_duration(std::vector<std::string> &_lines)
	{
		// Iterate through all elements in std::map
		
		double sum = this->get_duration();
		char buff[256];

		size_t maxidlen = 0;

		for (cv_time_class::iterator it = this->begin(); it != this->end(); it++)
		{
			if (maxidlen < it->first.length())
				maxidlen = it->first.length();
		}
		
		std::string fmt, txt;

		// Iterate backwarts to fill CONTAINER in right order
		// std::map pushes front
		// std::vec pushes back
		for (cv_time_class::iterator it = this->end(); it-- != this->begin();)
		{
			double mean = it->second.mean();
			double ratio = mean / sum;

			std::string txt(it->first);
			while (txt.size() < maxidlen) txt.push_back(' ');

			fmt = ">>   time measure evaluation :: " + txt;
			fmt = fmt + " (n_avg. %d): %4.3f s  [  %4.3f ]";

			sprintf(buff, fmt.c_str(), it->second.size(), mean, ratio);

			_lines.push_back(buff);
		}
	}

	inline void print_duration()
	{
		std::vector<std::string> lines;
		this->get_duration(lines);

		for (const std::string &_line : lines)
			std::cout << _line.c_str() << "\n";
	}

private:
	std::string m_current_id;
};








#endif // CV_TIME_MEASURE
